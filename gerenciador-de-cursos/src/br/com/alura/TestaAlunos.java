package br.com.alura;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class TestaAlunos {
	
	public static void main(String[] args) {
		 
		Collection<String> alunos = new HashSet<String>();
		alunos.add("Roberto Turini");
		alunos.add("Alberto Souza");
		alunos.add("Nico Steppat");
		alunos.add("Sergio Lopez");
		alunos.add("Renan Saggio");
		alunos.add("Mauricio Aniche");
		
		// Sets n�o aceitam elementos repetidos
		alunos.add("Renan Saggio");
		
		System.out.println(alunos.size());
		
		System.out.println("----------------------");
		
		for (String aluno : alunos) {
			System.out.println(aluno);
		}
		
		System.out.println("----------------------");
		
		alunos.forEach(aluno -> {
			System.out.println(aluno);
		});
		
		System.out.println("----------------------");
		
		System.out.println(alunos);
		
		System.out.println("----------------------");
		
		boolean marceloEstaMAtriculado = alunos.contains("Marcelo Oliveira");
		System.out.println(marceloEstaMAtriculado);
		
		System.out.println("----------------------");
		
		List<String> alunosOrdenados = new ArrayList<String>(alunos);
		Collections.sort(alunosOrdenados);
		System.out.println(alunosOrdenados);
		System.out.println(alunosOrdenados.get(3));
		
	}
	
}
