package br.com.alura;

import java.util.Iterator;
import java.util.Set;

public class TestaCursoComAluno {

	public static void main(String[] args) {

		Curso javaColecoes = new Curso("Dominando as colecoes do Java", "Paulo Silveira");

		javaColecoes.adiciona(new Aula("Trabalhando com ArrayList", 21));
		javaColecoes.adiciona(new Aula("Criando uma Aula", 20));
		javaColecoes.adiciona(new Aula("Modelando com colecoes", 24));

		Aluno a1 = new Aluno("Rodrigo Turini", 37584);
		Aluno a2 = new Aluno("Fernanda Paiva", 78254);
		Aluno a3 = new Aluno("Lu Santos", 26658);

		javaColecoes.matricula(a1);
		javaColecoes.matricula(a2);
		javaColecoes.matricula(a3);

		System.out.println("Alunos");
		System.out.println(javaColecoes.getAlunos());

		System.out.println("-----------------------");

		javaColecoes.getAlunos().forEach(aluno -> {
			System.out.println(aluno);
		});
		
		
		System.out.println("-----------------------------------");
		
		// javaColecoes.cancelaMatricula(a1);
		
		System.out.println("Esta matriculado o a2? " + javaColecoes.estaMatriculado(a2));
		
		Aluno turini = new Aluno("Rodrigo Turini", 37584);
		System.out.println("E esse turini, esta matriculado?");
		System.out.println(javaColecoes.estaMatriculado(turini));
		
		System.out.println("O a1 � igual ao turini?");
		System.out.println(a1 == turini);
		System.out.println(a1.equals(turini));
		
		System.out.println("------------------------------------");
		
		Set<Aluno> alunos = javaColecoes.getAlunos();
		Iterator<Aluno> iterador = alunos.iterator();
		
		while(iterador.hasNext()) {
			Aluno proximo = iterador.next();
			System.out.println(proximo);
		}

	}

}
