package br.com.alura;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeSet;

public class Curso {
	private String nome;
	private String nomeDoInstrutor;
	private List<Aula> aulas = new ArrayList<Aula>();
	// private Set<Aluno> alunos = new HashSet<Aluno>();
	// private Set<Aluno> alunos = new LinkedHashSet<Aluno>();
	private Set<Aluno> alunos = new TreeSet<Aluno>();
	
	// private Map<Integer, Aluno> matriculaParaAluno = new LinkedHashMap<>();
	private Map<Integer, Aluno> matriculaParaAluno = new HashMap<>();

	public Curso() {

	}

	public Curso(String nome, String nomeDoInstrutor) {
		this.nome = nome;
		this.nomeDoInstrutor = nomeDoInstrutor;
	}

	public List<Aula> getAulas() {
		return Collections.unmodifiableList(aulas);
	}

	public Set<Aluno> getAlunos() {
		return Collections.unmodifiableSet(alunos);
	}

	public String getNome() {
		return nome;
	}

	public String getNomeDoInstrutor() {
		return nomeDoInstrutor;
	}

	public void adiciona(Aula aula) {
		this.aulas.add(aula);
	}

	public void matricula(Aluno aluno) {
		this.alunos.add(aluno);
		this.matriculaParaAluno.put(aluno.getNumeroMatricula(), aluno);
	}

	public int getTempoTotal() {
		return this.aulas.stream().mapToInt(Aula::getTempo).sum();
	}

	@Override
	public String toString() {
		return "Curso [nome=" + nome + ", nomeDoInstrutor=" + nomeDoInstrutor + ", tempoTotal=" + this.getTempoTotal()
				+ ", aulas=" + aulas + "]";
	}
	
	public boolean estaMatriculado(Aluno aluno) {
		return this.alunos.contains(aluno);
	}

	public void cancelaMatricula(Aluno aluno) {
		alunos.remove(aluno);
	}

	public Aluno buscaMatriculado(int matricula) {
		if (!matriculaParaAluno.containsKey(matricula)) {
			throw new NoSuchElementException();
		}
		return matriculaParaAluno.get(matricula);
	}
}
