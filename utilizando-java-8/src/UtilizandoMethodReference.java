import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

public class UtilizandoMethodReference {

	public static void main(String[] args) {

		List<String> palavras = new ArrayList<String>();
		palavras.add("Alura Online");
		palavras.add("Editora Casa do C�digo");
		palavras.add("Caelum");

		// Collections.sort(palavras, comparador);

		// Utilizando classe anonima para ordenar
		// palavras.sort(new Comparator<String>() {
		//
		// @Override
		// public int compare(String s1, String s2) {
		// if (s1.length() < s2.length())
		// return -1;
		// if (s1.length() > s2.length())
		// return 1;
		// return 0;
		// }
		//
		// });

		// Utilizando lambda para ordenar
		palavras.sort((String s1, String s2) -> {
			if (s1.length() < s2.length())
				return -1;
			if (s1.length() > s2.length())
				return 1;
			return 0;
		});
		
		palavras.sort((String s1, String s2) -> {
			return Integer.compare(s1.length(), s2.length());
		});
		
		palavras.sort((String s1, String s2) -> Integer.compare(s1.length(), s2.length()));
		
		palavras.sort(Comparator.comparing(s -> s.length()));
		
		// Method reference = String::length
		palavras.sort(Comparator.comparing(String::length));
		
		// Desmebrando a logica acima
		Function<String, Integer> funcao = s -> s.length();
		Comparator<String> comparador = Comparator.comparing(funcao);
		palavras.sort(comparador);

		System.out.println(palavras);

		palavras.forEach(p -> {
			System.out.println(p);
		});
	}

}
