import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class UtilizandoStreams2 {

	public static void main(String[] args) {
		List<Curso> cursos = new ArrayList<Curso>();
		cursos.add(new Curso("Python", 45));
		cursos.add(new Curso("JavaScript", 150));
		cursos.add(new Curso("Java 8", 113));
		cursos.add(new Curso("C", 55));
		
		cursos.sort(Comparator.comparing(c -> c.getAlunos()));
		
		cursos.stream().filter(c -> c.getAlunos() >= 100)
			.findAny()
			.ifPresent(c -> System.out.println(c.getNome()));
		
		System.out.println("-------------------");
		
		cursos.stream().filter(c -> c.getAlunos() >= 100)
		.mapToInt(c  -> c.getAlunos())
		.average()
		.ifPresent(c -> System.out.println(c));
		
		System.out.println("-------------------");
		
		cursos = cursos.stream().filter(c -> c.getAlunos() >= 100)
		.collect(Collectors.toList());
		
		System.out.println("-------------------");
		
		cursos.stream().filter(c -> c.getAlunos() >= 100)
		.collect(Collectors.toMap(
				c -> c.getNome(),
				c -> c.getAlunos()))
		.forEach((nome, alunos) -> System.out.println("[nome: " + nome + ", alunos: " + alunos + "]"));;
		
	}

}
