import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class NovaApiDatas {
	public static void main(String[] args) {
        LocalDate hoje = LocalDate.now();
        System.out.println(hoje);
        
        System.out.println("-------------------------");
        
        LocalDate olimpiadasRio = LocalDate.of(2016, Month.JUNE, 5);
        
        System.out.println(olimpiadasRio);
        
        System.out.println("-------------------------");
        
        Period periodo = Period.between(hoje, olimpiadasRio);
        System.out.println("[ Passaram-se " + periodo.getYears() + " anos e " + periodo.getDays() + " dias. ]");
        
        System.out.println("-------------------------");
        
        LocalDate novaOlimpiada = olimpiadasRio.plusYears(4);
        System.out.println(novaOlimpiada);
        
        DateTimeFormatter formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String dataFormatada = formatador.format(novaOlimpiada);
        
        System.out.println(dataFormatada);
        
        System.out.println("-------------------------");
        
        DateTimeFormatter formatadorComHoras = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm");
        
        LocalDateTime agora = LocalDateTime.now();
        System.out.println(agora.format(formatadorComHoras));
        
        System.out.println("-------------------------");
        
        LocalTime data1 = LocalTime.now();
        LocalTime data2 = LocalTime.now().plusHours(3);
        
        System.out.println("[ data1 : " + data1 + " X " + "data2: " + data2 + " ]");
        
        System.out.println(data1.isAfter(data2));
        
        System.out.println(data1.isBefore(data2));
        
        ZonedDateTime dataComZone = ZonedDateTime.now();
        System.out.println(dataComZone);
        
	}
}
