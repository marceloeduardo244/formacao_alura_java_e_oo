import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class UtilizandoStreams {

	public static void main(String[] args) {
		List<Curso> cursos = new ArrayList<Curso>();
		cursos.add(new Curso("Python", 45));
		cursos.add(new Curso("JavaScript", 150));
		cursos.add(new Curso("Java 8", 113));
		cursos.add(new Curso("C", 55));
		
		cursos.sort(Comparator.comparing(c -> c.getAlunos()));
		System.out.println("Sem filtro");
		cursos.forEach(c -> System.out.println(c.getNome()));
		
		System.out.println("------------------------");
		System.out.println("Com filtro 1");
		
		
		cursos.stream()
			.filter(c -> c.getAlunos() >= 100)
			.forEach(c -> System.out.println(c.getNome()));
		
		System.out.println("------------------------");
		System.out.println("Com filtro 2");
		
		cursos.stream()
		.filter(c -> c.getAlunos() >= 100)
		.map(c -> c.getAlunos())
		.forEach(total -> System.out.println(total));
		
		System.out.println("------------------------");
		System.out.println("Com filtro 3");

		int totalDeAlunosFiltrado = cursos.stream()
		.filter(c -> c.getAlunos() >= 100)
		.mapToInt(c -> c.getAlunos())
		.sum();
		
		System.out.println(totalDeAlunosFiltrado);
		
		System.out.println("------------------------");
		
	}

}
