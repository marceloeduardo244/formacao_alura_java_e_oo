package br.com.bytebank.banco.teste;
import br.com.bytebank.banco.modelo.CalculadorImposto;
import br.com.bytebank.banco.modelo.Cliente;
import br.com.bytebank.banco.modelo.ContaCorrente;
import br.com.bytebank.banco.modelo.SeguroDeVida;

public class TestaCalculoImposto {

	public static void main(String[] args) {
		Cliente ana = new Cliente();
		ana.setNome("Ana");
		ContaCorrente cc = new ContaCorrente();
		cc.setTitular(ana);
		cc.deposita(1000);

		Cliente pedro = new Cliente();
		pedro.setNome("Pedro");
		SeguroDeVida sv = new SeguroDeVida();
		sv.setTitular(pedro);
		sv.deposita(2000);

		CalculadorImposto ci = new CalculadorImposto();
		ci.agregaImposto(cc);

		System.out.println(ci.getTributo());

		ci.agregaImposto(sv);

		System.out.println(ci.getTributo());
	}

}
