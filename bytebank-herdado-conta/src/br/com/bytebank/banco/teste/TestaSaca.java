package br.com.bytebank.banco.teste;
import br.com.bytebank.banco.modelo.Conta;
import br.com.bytebank.banco.modelo.ContaCorrente;
import br.com.bytebank.banco.modelo.SaldoInsuficienteException;

public class TestaSaca {

	public static void main(String[] args) {
		Conta conta = new ContaCorrente(123, 312);

		conta.deposita(200.0);
		try {
			conta.saca(201.0);
		} catch (SaldoInsuficienteException e) {
			System.out.println(e);
		}

		System.out.println(conta.getSaldo());

	}

}
