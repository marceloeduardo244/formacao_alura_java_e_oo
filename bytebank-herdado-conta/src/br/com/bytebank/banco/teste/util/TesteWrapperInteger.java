package br.com.bytebank.banco.teste.util;

import java.util.ArrayList;
import java.util.List;

public class TesteWrapperInteger {

	public static void main(String[] args) {
		int[] idades = new int[5];
		String[] nomes = new String[5];
		
		int idade = 29; // converte para Integer ao add na lista
		
		Integer idadeRef = Integer.valueOf(idade);
		
		System.out.println(idadeRef.doubleValue());
		
		int valor = idadeRef.intValue();
		
		List<Integer> numeros = new ArrayList<Integer>();
		numeros.add(idadeRef);
		
		String s = args[0]; //"10"
		
		// Integer numero = Integer.valueOf(s);
		int numero = Integer.parseInt(s);
		System.out.println(numero);
		
        Integer ref = Integer.valueOf("3");
        ref++;
        System.out.println(ref);
	}

}
