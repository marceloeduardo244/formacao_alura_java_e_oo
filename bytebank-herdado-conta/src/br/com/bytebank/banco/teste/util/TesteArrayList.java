package br.com.bytebank.banco.teste.util;

import java.util.ArrayList;

import br.com.bytebank.banco.modelo.Conta;
import br.com.bytebank.banco.modelo.ContaCorrente;

public class TesteArrayList {

	public static void main(String[] args) {
		ArrayList<Conta> lista = new ArrayList<>();

		Conta cc1 = new ContaCorrente(22, 11);
		Conta cc2 = new ContaCorrente(44, 55);
		Conta cc3 = new ContaCorrente(66, 77);
		Conta cc4 = new ContaCorrente(88, 99);
		Conta cc5 = new ContaCorrente(1010, 1111);
		Conta cc6 = new ContaCorrente(1212, 1313);

		lista.add(cc1);
		lista.add(cc2);
		lista.add(cc3);
		lista.add(cc4);
		lista.add(cc5);
		lista.add(cc6);

		System.out.println(lista.size());

		System.out.println(lista.get(3));

		Conta ref = lista.get(3);

		System.out.println(ref.getNumero());

		lista.remove(3);

		System.out.println(lista.size());

		Conta ref2 = lista.get(3);

		System.out.println(ref2.getNumero());


		// modo old do la�o

		for (Conta oRef : lista) {
			System.out.println(oRef);
		}

		System.out.println("----------------------");

		for (Object oRef : lista) {
			System.out.println(oRef);
		}

	}

}
