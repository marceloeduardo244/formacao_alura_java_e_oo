package br.com.bytebank.banco.teste.util;

import java.util.LinkedList;

import br.com.bytebank.banco.modelo.Conta;
import br.com.bytebank.banco.modelo.ContaCorrente;

public class TesteLinkedList {

	public static void main(String[] args) {
		LinkedList<Conta> lista = new LinkedList<Conta>();

		Conta cc1 = new ContaCorrente(22, 11);
		Conta cc2 = new ContaCorrente(44, 55);

		Conta cc3 = new ContaCorrente(66, 77);
		Conta cc4 = new ContaCorrente(88, 99);

		lista.add(cc1);
		lista.add(cc2);
		
		lista.add(cc3);
		lista.add(cc4);
		
		lista.remove(cc3);

		boolean exist = lista.contains(cc3);

		System.out.println("Ja existe: " + exist);

		for (Conta conta : lista) {
			if (conta.equals(cc3)) {
				System.out.println("Ja tenho essa conta");
			}
		}

		for (Object oRef : lista) {
			System.out.println(oRef);
		}

	}

}
