package br.com.bytebank.banco.teste.util;

import java.util.Vector;

import br.com.bytebank.banco.modelo.Conta;
import br.com.bytebank.banco.modelo.ContaCorrente;

public class TesteVector {

	public static void main(String[] args) {
		Vector<Conta> lista = new Vector<Conta>();

		Conta cc1 = new ContaCorrente(22, 11);
		Conta cc2 = new ContaCorrente(44, 55);

		Conta cc3 = new ContaCorrente(66, 77);
		Conta cc4 = new ContaCorrente(44, 55);

		lista.add(cc1);
		lista.add(cc2);

		boolean exist = lista.contains(cc4);

		System.out.println("Ja existe: " + exist);

		for (Conta conta : lista) {
			if (conta.equals(cc2)) {
				System.out.println("Ja tenho essa conta");
			}
		}

		for (Object oRef : lista) {
			System.out.println(oRef);
		}

	}

}
