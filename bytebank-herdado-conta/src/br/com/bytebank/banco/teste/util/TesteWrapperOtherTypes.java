package br.com.bytebank.banco.teste.util;

import java.util.ArrayList;
import java.util.List;

public class TesteWrapperOtherTypes {

	public static void main(String[] args) {
	
		Integer idadeRef = Integer.valueOf(29); //autoboxing
		System.out.println(idadeRef.doubleValue()); //unboxing
		
		Double dRef = new Double(3.2);
		System.out.println(dRef.doubleValue());
		
		Boolean bRef = Boolean.FALSE;
		System.out.println(bRef);
		
		Number refNumero = Double.valueOf(29.8);
		System.out.println(refNumero.doubleValue());
		
		Number refNumeroF = Float.valueOf(29.8f);
		System.out.println(refNumeroF.floatValue());
		
		List<Number> lista = new ArrayList<Number>();
		lista.add(refNumeroF);
		lista.add(dRef);
	}

}
