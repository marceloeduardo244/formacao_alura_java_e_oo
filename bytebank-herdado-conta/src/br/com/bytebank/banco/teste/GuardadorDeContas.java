package br.com.bytebank.banco.teste;

import br.com.bytebank.banco.modelo.Conta;

public class GuardadorDeContas {
	private Conta[] referencias;
	int posicaoLivreNoArray = 0;
	int tamanhoDoArray = 10;

	public GuardadorDeContas() {
		this.referencias = new Conta[10];
	}

	public void adicionaConta(Conta ref) {
		if (this.posicaoLivreNoArray <= this.tamanhoDoArray - 1) {
			this.referencias[this.posicaoLivreNoArray] = ref;
			System.out.println("A conta: " + ref.getNumero() + " foi adicionada com sucesso.");
			this.posicaoLivreNoArray++;
		} else {
			System.out.println("O Array de contas esta cheio!");
		}

	}

	public int getTamamhoDoArray() {
		return this.posicaoLivreNoArray;
	}

	public Conta getReferencia(int pos) {
		return this.referencias[pos];
	}
}
