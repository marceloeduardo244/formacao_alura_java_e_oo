package br.com.bytebank.banco.teste;

import br.com.bytebank.banco.modelo.ContaCorrente;
import br.com.bytebank.banco.modelo.ContaPoupanca;
import br.com.bytebank.banco.modelo.SaldoInsuficienteException;


public class TesteContas {

	public static void main(String[] args) {
		// classe abstratas n�o poder ser isntanciadas
		// Conta c = new Conta();

		ContaCorrente cc = new ContaCorrente(111, 222);
		cc.deposita(100);

		ContaPoupanca cp = new ContaPoupanca(333, 444);
		cp.deposita(200);

		try {
			cc.transfere(10, cp);
		} catch (SaldoInsuficienteException e) {
			System.out.println(e);
		}
		System.out.println("saldo CC" + cc.getSaldo());
		System.out.println("saldo CP" + cp.getSaldo());
	}

}
