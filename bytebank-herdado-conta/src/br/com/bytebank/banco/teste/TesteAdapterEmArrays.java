package br.com.bytebank.banco.teste;

import br.com.bytebank.banco.modelo.Conta;
import br.com.bytebank.banco.modelo.ContaCorrente;

public class TesteAdapterEmArrays {

	public static void main(String[] args) {
		GuardadorDeContas guardador = new GuardadorDeContas();

		Conta cc1 = new ContaCorrente(22, 11);
		Conta cc2 = new ContaCorrente(44, 55);
		Conta cc3 = new ContaCorrente(66, 77);
		Conta cc4 = new ContaCorrente(88, 99);
		Conta cc5 = new ContaCorrente(1010, 1111);
		Conta cc6 = new ContaCorrente(1212, 1313);
		Conta cc7 = new ContaCorrente(1414, 1515);
		Conta cc8 = new ContaCorrente(1616, 1717);
		Conta cc9 = new ContaCorrente(1818, 1919);
		Conta cc10 = new ContaCorrente(2020, 2121);
		Conta cc11 = new ContaCorrente(2222, 2323);

		guardador.adicionaConta(cc1);
		System.out.println(guardador.getTamamhoDoArray());
		guardador.adicionaConta(cc2);
		System.out.println(guardador.getTamamhoDoArray());
		guardador.adicionaConta(cc3);
		System.out.println(guardador.getTamamhoDoArray());
		guardador.adicionaConta(cc4);
		System.out.println(guardador.getTamamhoDoArray());
		guardador.adicionaConta(cc5);
		System.out.println(guardador.getTamamhoDoArray());
		guardador.adicionaConta(cc6);
		System.out.println(guardador.getTamamhoDoArray());
		guardador.adicionaConta(cc7);
		System.out.println(guardador.getTamamhoDoArray());
		guardador.adicionaConta(cc8);
		System.out.println(guardador.getTamamhoDoArray());
		guardador.adicionaConta(cc9);
		System.out.println(guardador.getTamamhoDoArray());
		guardador.adicionaConta(cc10);
		System.out.println(guardador.getTamamhoDoArray());
		guardador.adicionaConta(cc11);
		System.out.println(guardador.getTamamhoDoArray());

		System.out.println(guardador.getReferencia(5));
	}

}
