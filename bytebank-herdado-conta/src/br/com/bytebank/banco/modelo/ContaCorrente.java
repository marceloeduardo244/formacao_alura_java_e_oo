package br.com.bytebank.banco.modelo;

public class ContaCorrente extends Conta implements Tributavel {
	public ContaCorrente() {
		super();
	}

	public ContaCorrente(int agencia, int numero) {
		super(agencia, numero);
	}

    @Override
    public void saca(double valor) throws SaldoInsuficienteException {
    	super.saca(valor);
    }

	@Override
	public double getValorImposto() {
		return (super.getSaldo() / 100) * 2;
	}
}
