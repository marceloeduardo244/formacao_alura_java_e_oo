package br.com.bytebank.banco.modelo;

public class SeguroDeVida extends Conta implements Tributavel {
	public SeguroDeVida() {
		super();
	}

	public SeguroDeVida(int agencia, int numero) {
		super(agencia, numero);
	}

    @Override
    public void saca(double valor) throws SaldoInsuficienteException {
    	double valorASacar = valor + 0.20;
    	super.saca(valorASacar);
    }

	@Override
	public double getValorImposto() {
		return (super.getSaldo() / 100) * 2;
	}
}
