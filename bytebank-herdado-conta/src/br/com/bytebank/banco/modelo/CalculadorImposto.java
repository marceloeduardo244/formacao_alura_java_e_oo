package br.com.bytebank.banco.modelo;

public class CalculadorImposto {
	private double tributo = 0;

	public double getTributo() {
		return tributo;
	}

	public double agregaImposto(Tributavel t) {
		this.tributo += t.getValorImposto();

		return this.tributo;
	}
}
