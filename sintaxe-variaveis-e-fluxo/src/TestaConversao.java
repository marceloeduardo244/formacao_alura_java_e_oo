
public class TestaConversao {
	public static void main(String[] args) {
		double salario = 1270.50;
		int valor = (int) salario;
		
		System.out.println("salario original");
		System.out.println(salario);
		
		System.out.println("salario convertido");
		System.out.println(valor);
		
		long numeroGrande = 3245652654152L;
		short valorPequeno = 2121;
		byte b = 127;
	}
}
