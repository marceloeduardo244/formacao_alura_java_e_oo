
public class TestaPontoFlutuante {
	public static void main(String[] args) {
		double salario;
		salario = 37.2;
		
		System.out.println("salario");
		System.out.println(salario);
		
		/*int cabe dentro de um double*/
		
		double divisao = 3.14 / 2;
		System.out.println("divis�o");
		System.out.println(divisao);
		
		int outraDivisao = 5 / 2;
		System.out.println("outraDivisao");
		System.out.println(outraDivisao);
		
		double novaTentativa = 5 / 2;
		System.out.println("novaTentativa");
		System.out.println(novaTentativa);
		
		double terceiraTentativa = 5.0 / 2;
		System.out.println("terceiraTentativa");
		System.out.println(terceiraTentativa);
	}
}
