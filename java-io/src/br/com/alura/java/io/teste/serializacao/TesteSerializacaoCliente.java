package br.com.alura.java.io.teste.serializacao;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class TesteSerializacaoCliente {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		
		Cliente cliente = new Cliente();
		cliente.setCpf("03599930402");
		cliente.setNome("Marcelo");
		cliente.setProfissao("Desenvolvedor de Software");
		
		// String nome = "Nico Steppert";
		
		// Cria o bin�rio
//		 ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("cliente.bin"));
//		 oos.writeObject(cliente);
//		 oos.close();
		
		// L� o bin�rio
		 ObjectInputStream ois = new ObjectInputStream(new FileInputStream("cliente.bin"));
		 Cliente clienteRecuperadoDoBinario = (Cliente) ois.readObject();
		 ois.close(); 
		 System.out.println("clienteRecuperadoDoBinario: " + clienteRecuperadoDoBinario);
	}

}
