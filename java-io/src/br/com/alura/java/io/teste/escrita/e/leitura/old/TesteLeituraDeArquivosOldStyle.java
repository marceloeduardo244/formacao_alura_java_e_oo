package br.com.alura.java.io.teste.escrita.e.leitura.old;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class TesteLeituraDeArquivosOldStyle {

	public static void main(String[] args) throws IOException {
		// Fluxo entrada com um arquivo
		
		FileInputStream fis = new FileInputStream("lorem.txt");
		InputStreamReader isr = new InputStreamReader(fis);
		
		BufferedReader br = new BufferedReader(isr);
		
		String linha = br.readLine();
		
		while (linha != null) {
			System.out.println("Linha: " + linha);
			linha = br.readLine();
		}
		
		br.close();
	} 

}
