package br.com.alura.java.io.teste.escrita.e.leitura.old;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class TesteEscritaDeArquivos {

	public static void main(String[] args) throws IOException {
		// Fluxo entrada com um arquivo
		
		OutputStream fis = new FileOutputStream("lorem2.txt");
		Writer isr = new OutputStreamWriter(fis);
		
		BufferedWriter br = new BufferedWriter(isr);
		br.write("linha 1");
		br.newLine();
		br.write("linha 2");
		br.newLine();
		br.write("linha 3");
		
		br.close();
	} 

}
