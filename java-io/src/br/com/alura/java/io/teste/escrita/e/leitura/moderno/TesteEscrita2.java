package br.com.alura.java.io.teste.escrita.e.leitura.moderno;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class TesteEscrita2 {

	public static void main(String[] args) throws IOException {
		FileWriter fw = new FileWriter("lorem2moderno.txt");
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write("testeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee de escrita");
		bw.newLine();
		bw.newLine();
		bw.write("testeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee de escrita fim");
		
		bw.close();
	}

}
