package br.com.alura.java.io.teste.escrita.e.leitura.old;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;

public class TesteCopiandoEColandoArquivos {

	public static void main(String[] args) throws IOException {
		// Fluxo entrada com um arquivo
		
		// Leitura
		InputStream fis = new FileInputStream("lorem.txt");
		Reader isr = new InputStreamReader(fis);
		BufferedReader br = new BufferedReader(isr);
		
		// Escrita
		OutputStream fos = new FileOutputStream("lorem-copiado.txt");
		Writer osr = new OutputStreamWriter(fos);
		BufferedWriter bw = new BufferedWriter(osr);

		String linha = br.readLine();
		while (linha != null) {
			System.out.println("Linha: " + linha);
			bw.write(linha);
			bw.newLine();
			linha = br.readLine();
		}
		
		bw.close();
		br.close();
	} 

}
