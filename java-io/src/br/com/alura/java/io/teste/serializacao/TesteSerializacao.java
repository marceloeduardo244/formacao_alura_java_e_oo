package br.com.alura.java.io.teste.serializacao;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class TesteSerializacao {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		String nome = "Nico Steppert";
		
		// Cria o bin�rio
		// ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("objeto.bin"));
		// oos.writeObject(nome);
		// oos.close();
		
		// L� o bin�rio
		 ObjectInputStream ois = new ObjectInputStream(new FileInputStream("objeto.bin"));
		 String nomeRecuperadoDoBinario = (String) ois.readObject();
		 ois.close(); 
		 System.out.println("nomeRecuperadoDoBinario: " + nomeRecuperadoDoBinario);
	}

}
