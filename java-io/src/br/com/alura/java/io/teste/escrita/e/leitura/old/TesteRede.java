package br.com.alura.java.io.teste.escrita.e.leitura.old;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.Socket;

public class TesteRede {

	public static void main(String[] args) throws IOException {
		// Fluxo entrada com um arquivo
		
		Socket s = new Socket();
		
		InputStream fis = s.getInputStream();
		Reader isr = new InputStreamReader(fis);
		
		BufferedReader br = new BufferedReader(isr);
		
		String linha = br.readLine();
		
		while (linha != null) {
			System.out.println("Linha: " + linha);
			linha = br.readLine();
		}
		
		br.close();
		
		OutputStream fws = s.getOutputStream();
		Writer osr = new OutputStreamWriter(fws);
		
		BufferedWriter bw = new BufferedWriter(osr);
		bw.write("linha 1");
		bw.newLine();
		bw.write("linha 2");
		bw.newLine();
		bw.write("linha 3");
		
		bw.close();
	} 

}
