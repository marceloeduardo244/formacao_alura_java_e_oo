package br.com.alura.java.io.teste.escrita.e.leitura.moderno;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;

public class TesteEscrita3 {

	public static void main(String[] args) throws IOException {
		// FileWriter fw = new FileWriter("lorem2moderno.txt");
		// BufferedWriter bw = new BufferedWriter(fw);
		
		PrintStream ps = new PrintStream("lorem3moderno.txt");
		
		ps.println("testeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee de escrita");
		ps.println();
		ps.println("testeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee de escrita fim");
		
		ps.close();
		
		PrintWriter pw = new PrintWriter("lorem3moderno2.txt");
		
		pw.println("testeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee de escrita");
		pw.println();
		pw.println("testeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee de escrita fim");
		
		pw.close();
	}

}
