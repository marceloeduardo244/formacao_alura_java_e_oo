package bytebank;

import java.util.Objects;

public class Conta {
	private double saldo;
	private int agencia;
	private int numero;
	private String titular;
	
	public Conta() {
		super();
	}
	
	public Conta(int agencia, int numero, double saldo, String titular) {
		super();
		this.agencia = agencia;
		this.numero = numero;
		this.saldo = saldo;
		this.titular = titular;
	}
	public double getSaldo() {
		return saldo;
	}
	private void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public int getAgencia() {
		return agencia;
	}
	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getTitular() {
		return titular;
	}
	public void setTitular(String titular) {
		this.titular = titular;
	}
	
	void deposita(double valor) {
		if (valor > 0.0) {
			this.setSaldo(this.getSaldo() + valor);
		}
	}
	
	public boolean saca(double valor) {
		if(this.saldo >= valor) {
			this.setSaldo(this.getSaldo() - valor);
			System.out.println("Vc sacou: R$ " + valor);
			System.out.println("Seu novo saldo �: R$ " + this.getSaldo());
			return true;
		} 
		
		return false;
	}
	
	public boolean transfere(Conta contaDestinataria, double valor) {
		if (valor >= 0.0 && this.getSaldo() >= valor) {
			this.setSaldo(this.getSaldo() - valor);
			contaDestinataria.deposita(valor);
			System.out.println("transferencia executada com sucesso");
			return true;
		}
		System.out.println("erro, transferencia n�o executada");
		return false;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(agencia, numero);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Conta other = (Conta) obj;
		return agencia == other.agencia && numero == other.numero;
	}
	
	@Override
	public String toString() {
		return "Conta [saldo=" + saldo + ", agencia=" + agencia + ", numero=" + numero + ", titular=" + titular + "]";
	}
	
}
