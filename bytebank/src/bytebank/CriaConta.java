package bytebank;

public class CriaConta {

	public static void main(String[] args) {
		Conta contaDoMarcelo = new Conta(4, 123, 0.0, "Marcelo Eduardo Silva Oliveira");
		
		// contaDoMarcelo.setSaldo(200.4);
		
		System.out.println("conta do Marcelo");
		System.out.println(contaDoMarcelo.toString());
		
		Conta contaDaJoana = new Conta(4, 321, 0.0, "Joana de Nobrega");
		
		// contaDaJoana.setSaldo(102.6);
		
		System.out.println("conta do Joana");
		System.out.println(contaDaJoana.toString());
		
		Conta contaComValoresDefault = new Conta();
		
		System.out.println("contaComValoresDefault");
		System.out.println(contaComValoresDefault.toString());

	}

}
