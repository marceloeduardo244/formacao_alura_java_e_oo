package bytebank;

public class TestaMetodo {

	public static void main(String[] args) {
		Conta contaDaAna = new Conta();
		
		System.out.println("conta da ana 1");
		System.out.println(contaDaAna.getSaldo());
		
		contaDaAna.deposita(200.50);
		
		System.out.println("conta da ana 2");
		System.out.println(contaDaAna.getSaldo());
		
		if (contaDaAna.saca(199)) {
			System.out.println("opera��o ocorreu com sucesso");
		} else {
			System.out.println("Saldo insuficiente");
		}
		
		if (contaDaAna.saca(50)) {
			System.out.println("opera��o ocorreu com sucesso");
		} else {
			System.out.println("Saldo insuficiente");
		}
		
		Conta contaDoMarcelo = new Conta();
		Conta contaDoJoao = new Conta();
		
		contaDoJoao.deposita(300);
		
		contaDoJoao.transfere(contaDoMarcelo, 55.9);
		
		System.out.println("conta do jo�o");
		System.out.println(contaDoJoao.getSaldo());
		
		System.out.println("conta do marcelo");
		System.out.println(contaDoMarcelo.getSaldo());
		
	}

}
