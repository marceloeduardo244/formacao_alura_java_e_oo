package bytebank;

public class TesteReferencias {

	public static void main(String[] args) {
		Conta primeiraConta = new Conta();
		// primeiraConta.setSaldo(300);
		
		System.out.println("valores primeira conta");
		System.out.println(primeiraConta);
		
		Conta segundaConta = primeiraConta;
		System.out.println("valor segundaConta");
		System.out.println(segundaConta);
		
		// segundaConta.setSaldo(100);
		
		System.out.println("valores primeira conta 2");
		System.out.println(primeiraConta);
		
		System.out.println("valor segundaConta 2");
		System.out.println(segundaConta);

	}

}
