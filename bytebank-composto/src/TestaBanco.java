
public class TestaBanco {

	public static void main(String[] args) {
		Cliente marcelo = new Cliente("Marcelo Eduardo", "034.979.388-01", "Programador");
		
		System.out.println("cliente marcelo");
		System.out.println(marcelo);
		
		Conta contaDoMarcelo = new Conta();
		contaDoMarcelo.deposita(100);
		contaDoMarcelo.setTitular(marcelo);
		
		System.out.println("conta do Marcelo");
		System.out.println(contaDoMarcelo);

	}

}
