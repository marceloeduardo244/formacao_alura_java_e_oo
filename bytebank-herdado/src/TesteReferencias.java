
public class TesteReferencias {

	public static void main(String[] args) {
		Gerente g1 = new Gerente();
		g1.setNome("Pedro");
		g1.setSalario(2000.0);
		
		ControleBonificacao controle = new ControleBonificacao();
		
		controle.registra(g1);
		
		System.out.println(controle.getSoma());
		
//		Funcionario f1 = new Funcionario();
//		f1.setNome("Pedro");
//		f1.setSalario(2000.0);
//		
//		controle.registra(f1);
		
		System.out.println(controle.getSoma());
		
		EditorVideo ev1 = new EditorVideo();
		ev1.setNome("Joana");
		ev1.setSalario(2000.0);
		
		controle.registra(ev1);
		
		System.out.println(controle.getSoma());

	}

}
